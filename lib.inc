%define SYS_EXIT 60;
%define NEW_STRING '\n';
%define TAB 0x9;
%define SPACE 0x20;

section .text
exit:
    xor rax, SYS_EXIT
    syscall

string_length:
    mov rax, -1
    .loop:
        inc rax
            cmp byte[rdi+rax], 0
            je .end
            jmp .loop
        .end:
            ret

print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall    
    ret

print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret


print_newline:
    mov rdi, NEW_STRING
    jmp print_char


print_uint:
    mov rax, rdi
    mov rdi, 10
    mov rsi, rsp
    dec rsp
    mov [rsp], byte 0
    .loop:
        xor rdx, rdx
        div rdi
        add rdx, 48
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .end
        jmp .loop
    .end:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsp
        ret

print_int:
    cmp rdi, 0
    jnl .base
    push rdi
    mov rdi, 45
    call print_char
    pop rdi
    neg rdi
    .base:
        jmp print_uint

string_equals:
    mov rax, -1
    .loop:
        inc rax
        mov r8b, byte[rdi + rax]
        cmp r8b, byte[rsi + rax]
        je .check_zero
        mov rax, 0
        ret
    .check_zero:
        cmp r8b, 0
        jne .loop
        mov rax, 1
        ret

read_char:
    xor rax, rax
    push rax
    xor rdi, rdi
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .reading:
        call read_char
        cmp rax, SPACE
        je .skip
        cmp rax, TAB
        je .skip
        cmp rax, NEW_STRING
        je .skip
        mov [r12 + r14], rax
        inc r14
        cmp r14, r13
        je .check_buffer
        jmp .reading
    .check_buffer:
        call read_char
        cmp rax, 0
        je .end_success
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret
    .end_success:
        mov byte[r12+r14], 0
        mov rdi, r12
        call string_length
        mov rdx,rax
        mov rax,r12
        pop r14
        pop r13
        pop r12
        ret
    .skip:
        cmp r14, 0
        je .reading
        jmp .end_success

parse_uint:
    push rdi
    call string_length
    pop rdi
    mov rcx, rax
    mov rsi, rdi
    xor rdx, rdx
    xor rax, rax
    .pars:
        xor  rdi, rdi
        mov  dil, byte[rsi + rdx]
        cmp  dil, 48
        jb   .exit
        cmp  dil, 57
        ja   .exit
        sub  dil, 48
        imul rax, 10
        add  rax, rdi
        inc  rdx
        dec  rcx
        jnz  .pars
    .exit:
        ret

parse_int:
    cmp byte[rdi], 45
    je .not_base
    call parse_uint
    ret
    .not_base:
        inc rdi
        call parse_uint
        cmp rdx,0
        jne .success
        xor rax, rax
        ret
    .success:
    inc rdx
    neg rax
    ret

string_copy:
    xor r8, r8
    push rdi
    call string_length
    pop rdi
    inc rax
    cmp rdx, rax
    jl .error
    .loop:
        mov cl, byte[rdi + r8]
        mov byte[rsi + r8], cl
        inc r8
        cmp r8, rax
        jl .loop
        dec rax
        ret
    .error:
        mov rax, 0
        ret
